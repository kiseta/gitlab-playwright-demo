# Run Playwright Tests via GitLab CI/CD Pipeline. Use project-level variables. Save test results to artifacts viewable via GitLab Pages.

![Run Playwright Tests via GitLab CI/CD Pipeline](img/image01.png)

In this project you will:
* create a GitLab repository
* install Playwright + create basic tests
* configure yaml file to run tests via GitLab CI/CD pipeline 
* use GitLab project-level variables 
* save Playwright test results as pipeline artefacts 
* view the artifacts via GitLab pages.

# Step 1: Create new GitLab Project
GitLab: Create a new GitLab project repository and clone it to your local machine.
# Step 2: Install Playwright
On your local machine open the cloned GitLab repo in Visual Studio Code (VSCode).
Create a `.gitignore` file and add the following directories:
```
node_modules/
test-results/
tests-examples/
playwright-report/
playwright/.cache/
```
# Step 3: Install Playwright
* In the Terminal window in VSCode, install Playwright using the command: `npm init playwright@latest`
* During installation select your preferred language `>TypeScript` or `>JavaScript` (example in this guide are shown in JavaScript) and accept other prompts with default selections.

* After the installation, Playwright generates example tests including a test file named `tests/example.spec.js` that contains two basic tests navigating to the Playwright Home Page URL and doing validations:

```js
// example.spec.js 

const { test, expect } = require('@playwright/test');

test('has title', async ({ page }) => {
  await page.goto('https://playwright.dev/');
  // Expect a title "to contain" a substring.
  await expect(page).toHaveTitle(/Playwright/);
});

test('get started link', async ({ page }) => {
  await page.goto('https://playwright.dev/');
  // Click the get started link.
  await page.getByRole('link', { name: 'Get started' }).click();
  // Expects page to have a heading with the name of Installation.
  await expect(page.getByRole('heading', { name: 'Installation' })).toBeVisible();
});
```
* Run the tests with the command `npx playwright test`

# Step 4: Update the Playwright Config
First, replace hardcoded URL in tests with a reference to a global variable we will create in the Playwright Config file.

* Open the `playwright.config.js` file in VSCode and add the following:

```js
Object.assign(global, {
  BASE_URL: 'https://playwright.dev/',
});
```
* Optionally add this to `playwright.config.js` to always capture trace and generate video of the test execution
```js
  use: {
    trace: "on" /* Collect trace always. See https://playwright.dev/docs/trace-viewer */,
    video: "on" /* Record Video */,
    screenshot: "only-on-failure",
    headless: true,
    viewport: { width: 1900, height: 940 },
    launchOptions: {
      slowMo: 500,
    },
  },
```

# Step 5: Update the Playwright Test
* Open the `example.spec.js` file in VSCode.
* Replace the hardcoded URL https://playwright.dev/ with global.BASE_URL. This allows the test to navigate to the URL defined in the globalBASE_URL variable in Playwright Config.
* Here’s modified `example.spec.js` file

```js
// example.spec.js 

const { test, expect } = require('@playwright/test');

test('has title', async ({ page }) => {

  await page.goto(global.BASE_URL);

  // Expect a title "to contain" a substring.
  await expect(page).toHaveTitle(/Playwright/);
});

test('get started link', async ({ page }) => {

  await page.goto(global.BASE_URL);

  // Click the get started link.
  await page.getByRole('link', { name: 'Get started' }).click();

  // Expects page to have a heading with the name of Installation.
  await expect(page.getByRole('heading', { name: 'Installation' })).toBeVisible();
});
```
* Run the tests with the command `npx playwright test` to confirm the update.

# Step 6: Push updated Project to GitLab

Push all he updates to the project we’ve done so far to GitLab:
```bash
git add .
git commit -m "Add Playwright Tests"
git push
```
At this point our project files are pushed to remote repository but we don’t yet have a pipeline that would execute the tests we've just uploaded. Next we’re going to configure the pipeline and create a global variable for BASE_URL in GitLab project settings.

# Step 7: Define a Project Level Variable in GitLab
* Go to your remote project’s Settings in GitLab.
* Navigate to Settings > CI/CD section.
* Expand the Variables section.
* Click on "Add Variable".
* In the Key field, enter BASE_URL.
* In the Value field, enter the URL, i.e. https://playwright.dev/
* Click on "Add Variable" to save it.

![Add a Project Level Variable](img/image02.png)

# Step 8: Run the Test in GitLab CI/CD Pipeline
* In VSCode, create a .gitlab-ci.yml file in your local project root.
* Refer to Playwright Documentation for GitLab CI https://playwright.dev/docs/ci#gitlab-ci
* Define a job that runs the Playwright test and publishes run results folder playwright-report as artifacts with 2 days expiration period.
* We are also adding a shortcut link to script section to view execution results (saves 4 clicks) right from pipeline execution log.

```yaml
# Variables
variables:
  BASE_URL: $BASE_URL # variable defined in GitLab Project CI/CD Settings

# Define the stages for your pipeline
stages:
  - test

# Define the job for running Playwright tests
run_playwright_tests:
  stage: test
  image: mcr.microsoft.com/playwright:v1.39.0-jammy
  script:
    - npm ci # Install project dependencies
    - npx playwright test # Run your Playwright tests 
    - echo "https://$CI_PROJECT_NAMESPACE.gitlab.io/-/$CI_PROJECT_NAME/-/jobs/$CI_JOB_ID/artifacts/playwright-report/index.html" # print the URL to view the results
  allow_failure: true
  # Save Playwright test results as artifacts  
  artifacts:
    when: always
    paths:
      - playwright-report
    expire_in: 2 days

```

### Update Playwright Config file to reference a GitLab project-level variable

```js
Object.assign(global, {
  BASE_URL: process.env.BASE_URL,
});
```
### Commit and push the .gitlab-ci.yml file to the remote project in GitLab with the commands

```bash
git add .
git commit -m "Add GitLab CI/CD configuration"
git push
```

The pipeline should start automatically in the remote project in GitLab and run your test.

View the results of the pipeline execution in Build > Jobs menu

![View the results of the pipeline execution](img/image03.png)
![View the results of the pipeline execution](img/image04.png)

Now your pipeline in GitLab runs the automated script which takes the BASE_URL variable value from the project settings. This setup allows you to easily change the test URL without modifying the test script as well as adding other project level variables such as usernames and passwords.
View result by clicking the link from the pipeline execution log:

![View result by clicking the link from the pipeline execution log](img/image05.png)

NOTE: when adding secrets to your projects make sure ‘Mask Variable’ checkbox is selected to mask the values in the logs

Happy GitLab CI/CD testing! 🚀

### Helpful Commands

To push the updates skipping the pipline execution use the following command:
```bash
git push -o ci.skip

```

# Resources and References

### 1. Playwright Documentation for GitLab CI
https://playwright.dev/docs/ci#gitlab-ci

### 2. GitLab Playwright automation  Templates: 
https://gitlab.com/automation4175606/playwright-tests

### 3. Playwright and Gitlab CI: how to run your E2E tests
https://medium.com/@jeremie.fleurant/playwright-and-gitlab-ci-how-to-run-your-e2e-tests-42a51fd3e54e

